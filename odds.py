import datetime
import gzip
import urllib.request

from selenium.webdriver.chrome.options import Options
from selenium import webdriver

ODDS_LIST = []
SCORE_LIST = []


def get_html(url):
    headers = {'User-Agent': 'User-Agent:Mozilla/5.0'}
    request = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(request)
    html = un_gzip(response.read())
    # print_txt("1-html", html)
    return html


def un_gzip(data):
    try:
        # print('正在解压')
        data = gzip.decompress(data)
        # print('解压完毕')
    except:
        print('未经压缩，无需解压')
    return data.decode('gbk')


def print_txt(file_name, file_content):
    page_file = open(file_name + '.txt', 'w', encoding="utf-8")  # 以写的方式打开pageCode.txt
    page_file.write(file_content)  # 写入
    page_file.close()  # 开了记得关


def get_all_match(html):
    d1 = 'class="td_title01">分析</td>'
    s1 = html.partition(d1)[2]

    d2 = '</tbody>'
    s2 = s1.partition(d2)[0]

    return s2


def filter_match(browser, s):
    d1 = 'gy="英超,'
    a = s.partition(d1)

    if a[1] != '':
        s = a[2]

        t1 = s.partition(',')[0]
        s = s.partition(',')[2]
        # print(t1)

        t2 = s.partition('"')[0]
        s = s.partition('"')[2]
        # print(t2)

        # s = s.partition('class="clt1" >')[2]
        # score1 = s.partition('</a>')[0]
        # s = s.partition('</a>')[2]
        # s = s.partition('class="clt3" >')[2]
        # score2 = s.partition('</a>')[0]
        # s = s.partition('</a>')[2]
        #
        # print(t1 + " " + score1 + " - " + score2 + " " + t2)

        print(t1 + "-" + t2)

        s = s.partition('>亚</a>')[2]

        ss = s.partition('">欧</a>')[0]
        odds_url = ss.partition('href="')[2]
        print(odds_url)

        # browser = new_browser()
        get_odds(browser, odds_url)

        s = s.partition('">欧</a>')[2]

        filter_match(browser, s)

    else:
        print("no more")


def new_browser():
    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('window-size=1920x3000')  # 指定浏览器分辨率
    chrome_options.add_argument('--disable-gpu')  # 谷歌文档提到需要加上这个属性来规避bug
    chrome_options.add_argument('--hide-scrollbars')  # 隐藏滚动条, 应对一些特殊页面
    chrome_options.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度
    chrome_options.add_argument('--headless')  # 浏览器不提供可视化页面. linux下如果系统不支持可视化不加这条会启动失败
    # chrome_options.binary_location = r"D:\Program Files (x86)\Google\Chrome\Application\chrome.exe"

    browser = webdriver.Chrome(options=chrome_options)

    browser.set_page_load_timeout(10)

    # browser = webdriver.Chrome()
    return browser


def get_odds(browser, odds_url):

    if odds_url.startswith('//'):
        odds_url = 'http:' + odds_url

    try:
        print("start load url:" + odds_url)
        browser.get(odds_url)

        print("start print txt")
        print_txt("2.txt", browser.page_source)

        browser.find_element_by_xpath("//td[@title='澳门']/../td[3]/table/tbody/tr/td").click()

        score = browser.find_element_by_class_name("odds_hd_bf").text
        print(score)
        SCORE_LIST.append(score)

        odds_str = browser.find_element_by_xpath("//div[@id='tip_oz']/div[2]/table/tbody").text

        while odds_str == '':
            odds_str = browser.find_element_by_xpath("//div[@id='tip_oz']/div[2]/table/tbody").text

        odds_list = odds_str.split('\n')

        result = []

        for i, odd_str in enumerate(odds_list):
            odd = odd_str.split(' ')
            odd.pop(5)
            odd.pop(4)
            # print(odd)
            result.insert(0, odd)

        # print(result)

        if len(ODDS_LIST) > len(SCORE_LIST) - 1:
            print('List index error, please retry.')

        while len(ODDS_LIST) < len(SCORE_LIST) - 1:
            temp = []
            ODDS_LIST.append(temp)

        ODDS_LIST.append(result)

    except Exception:
        print('error, retry...except:' + str(Exception))
        get_odds(browser, odds_url)


# print('请输入日期(ex:2018-01-01):'),
# d = raw_input()

# d = '2018-12-21'
# url = "http://live.500.com/wanchang.php?e=" + d
# print(url)
# html = get_html(url)
# m_list = get_all_match(html)
# # print_txt(d + "-list", m_list)
# browser = new_browser()
# filter_match(browser, m_list)
# browser.close()

begin = datetime.date(2017, 1, 1)
end = datetime.date(2018, 12, 27)
browser = new_browser()
for i in range((end - begin).days + 1):
    d = begin + datetime.timedelta(days=i)
    url = "http://live.500.com/wanchang.php?e=" + str(d)
    print(url)
    html = get_html(url)
    m_list = get_all_match(html)
    # print_txt(d + "-list", m_list)
    filter_match(browser, m_list)

browser.close()

print('result:')
print(SCORE_LIST)
print(ODDS_LIST)
